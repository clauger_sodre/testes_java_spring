FROM openjdk:11
EXPOSE 8089
COPY ./target/mysqlsrest-1.1.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
