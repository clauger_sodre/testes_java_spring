package com.clauger.course.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.clauger.course.model.Pessoa;
import com.clauger.course.repository.PessoaRepository;
import com.clauger.course.services.GetDadosApi;

/*
 * https://loiane.com/2019/01/crud-rest-api-using-spring-boot-2-hibernate-jpa-and-mysql/
 * https://medium.com/thecodefountain/develop-a-spring-boot-and-mysql-application-and-run-in-docker-end-to-end-15b7cdf3a2ba
 */


@RestController // This means that this class is a Controller
@RequestMapping(path = "/app") // This means URL's start with /demo (after Application path)
public class PessoaMySqlController {
	@Autowired // injeta a interface
	private PessoaRepository pessoaRepository;

	@PostMapping(path ="/add") // Map ONLY POST Requests não pode ter espaço em bran
	public Pessoa create(@RequestBody Pessoa pessoa){
	    return pessoaRepository.save(pessoa);
	}

	

	@GetMapping(path ="/all")
	public @ResponseBody Iterable<Pessoa> getAllUsers() {
		return pessoaRepository.findAll();

	}

	@GetMapping(path = "/pessoa/{id}")
	public @ResponseBody Optional<Pessoa> findById(@PathVariable long id) {
		return pessoaRepository.findById(id);

	}
	
	@PutMapping(value="/pessoa/{id}")
	  public ResponseEntity<Pessoa> update(@PathVariable("id") long id,
	                                        @RequestBody Pessoa pessoa){
	    return pessoaRepository.findById(id)
	        .map(record -> {
	            record.setNome(pessoa.getNome());
	            record.setEmail(pessoa.getEmail());	 
	            record.setCpf(pessoa.getCpf());	
	            record.setSenha(pessoa.getSenha());
	            Pessoa updated = pessoaRepository.save(record);
	            return ResponseEntity.ok().body(updated);
	        }).orElse(ResponseEntity.notFound().build());
	  }
	@DeleteMapping(path ={"/pessoa/{id}"})
	  public ResponseEntity<?> delete(@PathVariable("id") long id) {
	    return pessoaRepository.findById(id)
	        .map(record -> {
	            pessoaRepository.deleteById(id);
	            return ResponseEntity.ok().build();
	        }).orElse(ResponseEntity.notFound().build());
	  }

	@PostMapping(path ={"/pessoa_api/{id}"})
	public @ResponseBody Pessoa pessoaApi(@PathVariable("id") String id) throws Exception {
		
		GetDadosApi pegaDados = new GetDadosApi();
		Pessoa pessoaOutraApi =pegaDados.buscaPessoasBancoMongo(id);
		//System.out.println("==============================");
		//System.out.println("Pessoa retornada: "+pessoaOutraApi);
		return pessoaOutraApi;
	}
	
	@RequestMapping(method= RequestMethod.GET, path={"/login"})
	public void login() {
		System.out.println("Logado");
		//return "entrar.html";
	}
	@RequestMapping(method= RequestMethod.GET, path={"/logout"})
	public void logout() {
		System.out.println("Deslogado");
		//return "entrar.html";
	}
}// Fim do metodo
