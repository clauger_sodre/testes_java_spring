package com.clauger.course.repository;

import org.springframework.data.repository.CrudRepository;

import com.clauger.course.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {


}
