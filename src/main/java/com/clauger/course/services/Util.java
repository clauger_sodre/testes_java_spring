package com.clauger.course.services;

import java.io.BufferedReader;
import java.io.IOException;
/*
 * Converte um json em String
 * 
 */
public class Util {
    public static String converteJsonEmString(BufferedReader buffereReader) throws IOException {
        String resposta, jsonEmString = "";
        while ((resposta = buffereReader.readLine()) != null) {
            jsonEmString += resposta;
        }
        return jsonEmString;
    }
}
