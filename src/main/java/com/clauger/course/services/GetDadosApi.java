package com.clauger.course.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.clauger.course.model.Pessoa;
import com.google.gson.Gson;

public class GetDadosApi {

	
	
	public Pessoa buscaPessoasBancoMongo(String id) throws Exception {
		final String webService = "http://localhost:3000/";
		final int codigoSucesso = 200;
		Pessoa pessoaJson = new Pessoa();
		//String urlParaChamada = webService + "add" + "/json";
		String urlParaChamada = webService + "cliente/"+id;
		System.out.println("==========================");
		System.out.println("URL para postar: "+urlParaChamada);
		try {
			URL url = new URL(urlParaChamada);
			HttpURLConnection conexao = (HttpURLConnection) url.openConnection();

			if (conexao.getResponseCode() != codigoSucesso)
				throw new RuntimeException("HTTP error code : " + conexao.getResponseCode());
			
			//System.out.println("Recebeu codigo ok");
			BufferedReader resposta = new BufferedReader(new InputStreamReader((conexao.getInputStream())));// Recebe o  Json
			//System.out.println("Resposta json: "+resposta);																				
			String jsonEmString = Util.converteJsonEmString(resposta);
			System.out.println("Json em string recebido: "+jsonEmString);
			Gson gson = new Gson();
			pessoaJson = gson.fromJson(jsonEmString, Pessoa.class);
			String json = new Gson().toJson(pessoaJson, Pessoa.class);
			
			System.out.println("Objeto pessoa criado pela busca: "+pessoaJson);

		} catch (Exception e) {
			throw new Exception("ERRO: " + e);
		}
		return pessoaJson;
	}
}
