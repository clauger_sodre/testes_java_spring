package com.clauger.course.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableWebSecurity
@EnableAuthorizationServer
@EnableResourceServer
@Configuration // Avisa para o Spring que essa é uma classe de configuração
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override 
	protected void configure(HttpSecurity http) throws Exception { //
	   http
	   .authorizeRequests()// requisições //
	   		.antMatchers("/swagger-ui.html")
	   				//.hasAnyRole("DEV")//Define quais paginas serão protegidas e quem pode acessar
	   				//.antMatchers("/app")
	   			.hasAnyRole("DEV") 
	   			.anyRequest() // para qualquer requisição
	   			.authenticated()// Para toda requisição o usuário precisa estarautenticado 
	  .and()
	  		.formLogin()// Autenticação através de um formulario de  login para criar uma nova pagina de // login coloque em src/main/resources/templates com nome dos imputs "username" // e "password"
	 	  // .loginPage("/app/login")//Avisa o spring que temos uma pagina de login propria 
	  		.permitAll()
	  .and()
	  		.logout()// avisa sobre o metodo deslogar
	  		.logoutSuccessUrl("/app/logout")// fala onde será o metodo deslogar
	  		.permitAll();// permite todos deslogarem
	  
	  }

	@Autowired
	private void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		builder.inMemoryAuthentication().withUser("clauger").password("123").roles("ADMIN", "USER", "PROD","DEV").and()
				.withUser("amanda").password("203040").roles("DEV");

	}

	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
}
