package com.clauger.course;
// Video com token oath https://www.youtube.com/watch?v=FOX0r52_hwE
// Conectar localmente spring.datasource.url=jdbc:mysql://localhost:8088/aulajpa
//Conectar no container spring.datasource.url=jdbc:mysql://mysql:3306/aulajpa
import org.springframework.boot.CommandLineRunner;
/*
 * https://arthur-almeida.medium.com/consumindo-uma-api-de-maneira-simples-com-java-2a386010e4b9
 * instale a dependencia com.google.code.gson no Maven
 * https://www.javaavancado.com/manipulando-json-com-google-gson/
 * 
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.clauger.course.model.Pessoa;
import com.google.common.base.Predicates;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/*
 * Só roda se o node estiver up pq requisita no node
 * 
 */
@SpringBootApplication
@Configuration
@EnableSwagger2
public class MySqlRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySqlRestApplication.class, args);

	}

	// Cria o Swagger
	@Bean
	public Docket api() {
		// @formatter:off
	        //Register the controllers to swagger
	        //Also it is configuring the Swagger Docket
	        return new Docket(DocumentationType.SWAGGER_2).select()
	                // .apis(RequestHandlerSelectors.any())
	                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
	                // .paths(PathSelectors.any())
	                // .paths(PathSelectors.ant("/swagger2-demo"))
	                .build();
	        // @formatter:on                                          
	}

	// Para consumir Json de outra API
	// private static final Logger log =
	// LoggerFactory.getLogger(MySqlRestApplication.class);

	/*
	 * @Bean public RestTemplate restTemplate(RestTemplateBuilder builder) { return
	 * builder.build(); }
	 * 
	 * @Bean public CommandLineRunner run(RestTemplate restTemplate) throws
	 * Exception { return args -> { String webService = "http://localhost:3000";
	 * String rota = "/cliente"; String id = "/5ffb2e1f6032f3594733a93f"; String
	 * query = webService + rota + id;
	 * 
	 * System.out.println("Rota a ser executada: " + query);
	 * 
	 * Pessoa pessoa = restTemplate.getForObject(query, Pessoa.class);
	 * System.out.println("Resultado da Chamada REST: " + pessoa.toString()); }; }
	 */

}
